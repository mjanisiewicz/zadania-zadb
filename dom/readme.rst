
Praca domowa #B
===============

Korzystając z dowolnego frameworka webowego stworzyć galerię zdjęć dla wielu uzytkowników.
Galeria powinna mieć następujące funkcjonalności:

* Dodawanie pojedynczego zdjęcia
* Dodawanie pliku zip z wieloma zdjęciami, które aplikacja rozpakowuje i wrzuca do galerii
* Tworzenia katalogów i przenoszenie zdjęć między nimi
* Wyświetlanie zawartości katalogu w postaci miniatur o różnych wielkościach (co najmniej 3 do dyspozycji).
  Miniatury powinny być przeskalowanymi obrazkami po stronie serwera a nie pełnowymiarowymi tylko przeskalowanymi
  po stronie przeglądarki za pomocą styli, atrybutów html czy javascriptu.
* Zapisywanie pojedynczego zdjęcia na dysk.
* Zapisywanie wszystkich zdjęć z wybranego katalogu w postaci pliku zip.

Interfejs ma być przyjazny dla użytkownika i w żadnym wypadku się nie zacinać.
W przypadku długotrwałych zadań (np. pakowanie lub rozpakowywanie pliku zip) należy wyświetlić
użytkownikowi odpowiednią informację oraz pasek postępu.
