__author__ = 'Marek'
import random, timeit, uuid
from celery import group
import worker
import requests

def sort_random(n):
        return worker.sort_random.s(n)


def make_request():
        test = sort_random(random.randint(1, 5))
        return test


def requests_suite():
        results = [make_request() for i in xrange(2)]
        r = group(results)().get()
        merged = []
        for r_ in r:
            merged = merged + r_
            print merged
        return merged

merged = []
print 'Ten successive runs:',
for i in range(1, 11):
        arr = requests_suite()
        print arr