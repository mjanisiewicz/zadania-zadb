__author__ = 'Marek'
#!/usr/bin/env python
from celery import Celery
import time
import requests
celery = Celery('worker')

class Config:
        BROKER_URL='amqp://194.29.175.241'
        CELERY_RESULT_BACKEND = 'cache'
        CELERY_CACHE_BACKEND = 'memcached://194.29.175.241:11211'
        CELERY_DEFAULT_QUEUE = 'queue-p17'

celery.config_from_object(Config)

@celery.task
def sort_random(n):
    base = "http://www.random.org/integers/"
    params = {'num': n, 'min': 1, 'max': 1000, 'col': 1, 'base': 10, 'format': 'plain', 'rnd': 'new'}
    resp = requests.get(base, params=params).text
    numbers = resp.split('\n')
    numbers.pop()
    #print numbers
    numbers2 = []
    for number in numbers:
        numbers2.append(int(number))
    #print numbers2
    numbers2.sort()
    #print numbers2
    return numbers2

if __name__ == '__main__':
        celery.start()